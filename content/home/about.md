+++
# About/Biography widget.
widget = "about"
active = false
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 3

+++
We are a team led by Nathan Faivre, part of the Laboratoire de Psychologie & Neurocognition in Grenoble.  
<br>
We are interested in the interplay between perceptual and self consciousness.  
<br> 
We use psychophysics, neuroimaging (fMRI) and electrophysiology (EEG, iEEG, DBS) in various settings (see projects below).  
<br>
Reach out if you're interested in collaborating, we are hiring!
