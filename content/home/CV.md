+++
# About/Biography widget.
title = "CV"
widget = "CV_flip"
active = false
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 2

cv_pdf = "./files/cv.pdf"

# List your academic interests.
[interests]
  interests = [
    "Perceptual consciousness",
    "Self consciousness",
    "Metacognition", 
    "Schizophrenia"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "Postdoctoral fellow"
  institution = "Ecole Polytechnique Fédérale de Lausanne"
  year = 2014 

[[education.courses]]
  course = "Postdoctoral fellow"
  institution = "California Institute of Technology"
  year = 2012 

[[education.courses]]
  course = "PhD in cognitive neurosciences"
  institution = "Ecole Normale Supérieure"
  year = 2011
 
+++
