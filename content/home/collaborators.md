+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "projects"
active = true
date = 2016-04-20T00:00:00

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Main collaborators"
subtitle = "Click [here] (www.google.fr) for many other friends"
folder = "collaborators"
# Order that this section will appear in.
weight = 65
view = 1
+++

