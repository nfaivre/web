+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "custom"
active = false
date = 2016-04-20T00:00:00

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Teaching"
subtitle = ""

# Order that this section will appear in.
weight = 60

+++

#### 2018
* Perceptual and Self Consciousness  
(Master in Life Sciences, EPFL)


#### 2017 
* Perceptual and Self Consciousness  
(Master in Life Sciences, EPFL)


#### 2016 
* Cognition and Decision Making  
(Master economy and psychology, University Pantheon Sorbonne)


#### 2011 
* Introspection & Metacognition  
(Cogmaster, Collège de France)


#### 2010 
* Cellular Biology  
(Medical school, University Pierre et Marie Curie)


* Consciousness Access  
(Cogmaster, Collège de France)


#### 2009 
* Cellular Biology  
(Medical school, University Pierre et Marie Curie)


#### 2008
* Developmental Biology  
(Medical school, University Pierre et Marie Curie)


* Cellular Biology  
(Medical school, University Pierre et Marie Curie)

