+++
# Project title.
title = "New funding!"

# Date this page was created.
date = 2018-09-27T00:00:00

# Project summary to display on homepage.
summary = "ERC starting grant"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Optional external URL for project (replaces project detail page).
external_link = ""

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
  preview_only = true

+++

The project [**metAction**] (../../project/meta/) was awarded an ERC starting grant of 1.4M€ for five years!

We are looking for PhD students and postdocs, starting early 2019. More details [**here**] (../../team/m1/).
