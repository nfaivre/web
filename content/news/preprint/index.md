+++
# Project title.
title = "New preprint"

# Date this page was created.
date = 2018-10-15T00:00:00

# Project summary to display on homepage.
summary = "EEG-fMRI study"

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Optional external URL for project (replaces project detail page).
external_link = ""

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Center"
  preview_only = true

+++
After two years of work, we are happy to present our new study entitled [**Disentangling the origins of confidence in speeded perceptual judgments through multimodal imaging**] (https://www.biorxiv.org/content/early/2018/12/17/496877).  

Together with my great friends and colleagues [**Michael Pereira**] (https://people.epfl.ch/michael.pereira) and [**Iñaki Iturrate**] (https://people.epfl.ch/inaki.iturrate), we combined EEG, fMRI, and computational modeling on a perceptual decision-making task to unravel the origins of confidence.  

[**Check out the paper here**] (https://www.biorxiv.org/content/early/2018/12/17/496877). All data and scripts will be made available here soon. 

