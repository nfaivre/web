+++
title = "You?"
date = 2018-07-19T17:10:34+02:00
draft = false

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Project summary to display on homepage.
summary = "We're hiring!"



# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Does the project detail page use source code highlighting?
highlight = true

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Center"


  preview_only = true
+++
Between 2019 and 2024, we will be hiring two PhD students and two postdocs, fully funded by an [ERC starting grant] (https://erc.europa.eu/).  

One postdoc position starting in spring is already open. The next PhD position will open this fall, but we welcome master students and international fellowship applicants all year long.  

We are looking for highly motivated people interested in consciousness and metacognition, with good methodological skills in psychophysics, signal processing, statistics, and/or computational modeling.  

Proficiency with R, matlab, and/or python is required. 


[**Click here to get in touch!**] (../../#contact)  
Please send a brief message explaining your interest in the lab, including a link to your CV along with the names and contact information of two references.


