+++
title = "Olaf Blanke"
date = 2018-07-29T17:13:30+02:00
draft = false

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Project summary to display on homepage.
summary = "EPFL"


# Optional external URL for project (replaces project detail page).
external_link = "https://lnco.epfl.ch/"
 
# Does the project detail page use math formatting?
math = false

# Does the project detail page use source code highlighting?
highlight = true


# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"



+++

