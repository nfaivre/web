+++
title = "Perceptual consciousness"
date = 2018-08-21T17:10:34+02:00
draft = false

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Project summary to display on homepage.
summary = ""


# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Does the project detail page use source code highlighting?
highlight = true

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Center"
  preview_only = true

+++

Over the last 30 years, significant progress has been made to unravel the minimal set of neuronal events sufficient for a specific conscious episode to occur. The main paradigm to isolate such neural correlates of consciousness is based on a contrastive approach, comparing situations in which a conscious percept occurs with closely matched situations in which it does not.  

We take advantage of the possibility to record neural activity during the monitoring of epileptic patients to define electrocortical correlates of consciousness in Humans.  
 
Besides the cortex, several accounts suggest thalamo-cortical loops as a key mechanism for conscious access. Yet, evidence for subcortical signatures is much more scarce, in part due to the difficulty to measure subcortical structures with high spatio-temporal resolution in Humans able to provide subjective reports. We take advantage of the possibility to record neuronal activity in subcortical structures during surgeries for deep brain stimulation to test subcortical correlates of consciousness in Humans.  

**This research is conducted in collaboration with Grenoble Hospital, the Ecole Polytechnique Fédérale de Lausanne and West Virginia University.**
