+++
title = "Multisensory integration"
date = 2017-07-21T17:10:34+02:00
draft = false

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Project summary to display on homepage.
summary = ""


# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Does the project detail page use source code highlighting?
highlight = true

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Center"
  preview_only = true

+++
Cognitive neurosciences largely focus on vision, and the field of consciousness studies is no exception.  

Yet, we believe that a comprehensive scientific account of subjective experience must not neglect other exteroceptive and interoceptive signals as well as the role of multisensory interactions for perceptual and self-consciousness.  

In previous works, we tested whether signals from different sensory modalities can be integrated into multimodal representations (short answer: it depends!), and whether multimodal representations are equiavlent to unimodal representations for metacognitive judgments (short answer: yes!). We also assessed the contribution of multimodal representations to bodily self-consciousness.   


