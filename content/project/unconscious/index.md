+++
# Project title.
title = "Unconscious cognition"

# Date this page was created.
date = 2016-04-27T00:00:00

# Project summary to display on homepage.
summary = ""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Optional external URL for project (replaces project detail page).
external_link = ""

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
  preview_only = true

+++
One critical issue to understand consciousness concerns the distinction between conscious and unconscious processes.  

Drawing a complete picture of mental life requires not only to focus on the nature and properties of consciousness per se, but also, by exclusion, on the multitude of mechanisms and processes occurring without consciousness.  

By comparing situations in which consciousness happens with closely matched situations in which it does not (i.e., unconscious processing), we study both its functional and neural properties.  

This approach has not only been motivated by the need to understand the specificity of consciousness, but also by the will to characterize unconscious processes on their own.  

In the last years, we determined the limits and extents of unconscious visual processes relying on three main techniques that are visual masking, continuous flash suppression, and gaze-contingent crowding. 
