+++
# Project title.
title = "Metacognitive monitoring"

# Date this page was created.
date = 2018-09-27T00:00:00

# Project summary to display on homepage.
summary = ""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Optional external URL for project (replaces project detail page).
external_link = ""

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Center"
  preview_only = true

+++
Humans can monitor their own mental lives and build representations that contain knowledge about themselves. This capacity to introspect and report one’s own mental states, or in other words *knowing how much one knows*, is termed **metacognition**.  

Although metacognition is crucial to behave adequately in a complex environment, metacognitive judgments are often suboptimal. Specifically for neurological and psychiatric diseases, metacognitive failures are highly prevalent, with severe consequences in terms of quality of life.  

Our research aims to explain the determining factors of metacognitive failures. We think that metacognition does not operate in a vacuum but relies on the monitoring of signals from the body, and more specifically, on motor signals involved during action execution.  

We are now conducting experiments to test this idea, funded by a starting grant from the European Research Council.
