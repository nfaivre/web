+++
# Project title.
title = "Animal consciousness"

# Date this page was created.
date = 2018-07-27T00:00:00

# Project summary to display on homepage.
summary = ""

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Optional external URL for project (replaces project detail page).
external_link = ""

# profile = false

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. 
[image]
  # Caption (optional)
  caption = ""
  
  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
  preview_only = true
+++
Most studies on human consciousness rely on a combination of subjective reports and objective measures. Studying consciousness in beings unable to provide verbal reports (e.g., non-human animals, babies, neurological patients) is particularly challenging, and central to serious ethical issues.  

We do not perform experiments on non-human animals in the lab, but we are interested in the ethical and societal issues around animal consciousness.  

Recently, we reviewed the empirical evidence regarding animal consciousness with colleagues from the [French National Institute of Research in Agronomy] (http://www.inra.fr/) (see below).  

A book in French was published based on this report. **Check it** [**out**](http://www.quae.com/fr/r5324-la-conscience-des-animaux.html)!
