+++
title = "Self-grounded vision: Hand ownership modulates visual location through cortical β and γ oscillations"
date = "2017-01-01"
authors = ["N. Faivre", "J. Donz", "M. Scandola", "H. Dhanis", "J. Bello Ruiz", "F. Bernasconi", "R. Salomon", "O. Blanke"]
publication_types = ["1"]
publication = "Journal of Neuroscience, (37), 1, _pp. 11-22_, https://doi.org/10.1523/JNEUROSCI.0563-16.2016"
publication_short = "Journal of Neuroscience, (37), 1, _pp. 11-22_, https://doi.org/10.1523/JNEUROSCI.0563-16.2016"
abstract = ""
abstract_short = ""
image_preview = "" 
selected = true
projects = ["self","percept"]
tags = []
url_pdf = "files/reprint_JNeuro2017.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
