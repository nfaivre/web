+++
title = "Numerical Priming Between Touch and Vision Depends on Tactile Discrimination"
date = "2016-01-01"
authors = ["N. Faivre", "R. Salomon", "L. Vuillaume", "O. Blanke"]
publication_types = ["1"]
publication = "Perception, (45), 1-2, _pp. 114-124_, https://doi.org/10.1177/0301006615599129"
publication_short = "Perception, (45), 1-2, _pp. 114-124_, https://doi.org/10.1177/0301006615599129"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious","multisensory"]
tags = []
url_pdf = "http://journals.sagepub.com/doi/10.1177/0301006615599129"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
