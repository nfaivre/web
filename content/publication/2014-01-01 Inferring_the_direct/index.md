+++
title = "Inferring the direction of implied motion depends on visual awareness"
date = "2014-01-01"
authors = ["N. Faivre", "C. Koch"]
publication_types = ["1"]
publication = "Journal of Vision, (14), 4, https://doi.org/10.1167/14.4.4"
publication_short = "Journal of Vision, (14), 4, https://doi.org/10.1167/14.4.4"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious"]
tags = []
url_pdf = "https://jov.arvojournals.org/article.aspx?articleid=2121534"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
