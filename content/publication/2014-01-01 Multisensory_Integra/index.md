+++
title = "Multisensory Integration in Complete Unawareness: Evidence From Audiovisual Congruency Priming"
date = "2014-01-01"
authors = ["N. Faivre", "L. Mudrik", "N. Schwartz", "C. Koch"]
publication_types = ["1"]
publication = "Psychological Science, (25), 11, _pp. 2006-2016_, https://doi.org/10.1177/0956797614547916"
publication_short = "Psychological Science, (25), 11, _pp. 2006-2016_, https://doi.org/10.1177/0956797614547916"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious","multisensory"]
tags = []
url_pdf = "files/reprint_psychscience2014.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
url_custom = [{name = "SI", url = "files/reprint_psychscience2014_SI.pdf"}]
math = true
highlight = true
[header]
image = ""
caption = ""
+++
