+++
title = "Temporal structure coding with and without awareness"
date = "2014-01-01"
authors = ["N. Faivre", "C. Koch"]
publication_types = ["1"]
publication = "Cognition, (131), 3, _pp. 404-414_, https://doi.org/10.1016/j.cognition.2014.02.008"
publication_short = "Cognition, (131), 3, _pp. 404-414_, https://doi.org/10.1016/j.cognition.2014.02.008"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious"]
tags = []
url_pdf = "files/reprint_Cognition2014.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
