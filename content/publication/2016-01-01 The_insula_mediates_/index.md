+++
title = "The insula mediates access to awareness of visual stimuli presented synchronously to the heartbeat"
date = "2016-01-01"
authors = ["R. Salomon", "R. Ronchi", "J. Donz", "J. Bello-Ruiz", "B. Herbelin", "R. Martet", "N. Faivre", "K. Schaller", "O. Blanke"]
publication_types = ["1"]
publication = "Journal of Neuroscience, (36), 18, _pp. 5115-5127_, https://doi.org/10.1523/JNEUROSCI.4262-15.2016"
publication_short = "Journal of Neuroscience, (36), 18, _pp. 5115-5127_, https://doi.org/10.1523/JNEUROSCI.4262-15.2016"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["self","percept"]
tags = []
url_pdf = "files/reprint_JNeuro2016.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
