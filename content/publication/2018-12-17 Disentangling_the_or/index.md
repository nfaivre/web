+++
title = "Disentangling the origins of confidence in speeded perceptual judgments through multimodal imaging"
date = "2018-12-17"
authors = ["M. Pereira", "N. Faivre", "I. Iturrate", "M. Wirthlin", "L. Serafini", "S. Martin", "A. Desvachez", "O. Blanke", "D. Van De Ville","J. Millan"]
publication_types = ["1"]
publication = "bioRxiv, https://doi.org/10.1101/496877"
publication_short = "bioRxiv, https://doi.org/10.1101/496877"
abstract = ""
abstract_short = ""
image_preview = "featured.png"
selected = true
projects = ['meta']
tags = []
url_pdf = ""
url_preprint = "https://www.biorxiv.org/content/early/2018/12/17/496877"
url_code = "https://gitlab.com/nfaivre/analysis_public"
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
