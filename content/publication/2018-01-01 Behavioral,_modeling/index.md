+++
title = "Behavioral, modeling, and electrophysiological evidence for supramodality in human metacognition"
date = "2018-01-01"
authors = ["N. Faivre", "E. Filevich", "G. Solovey", "S. Kuhn", "O. Blanke"]
publication_types = ["1"]
publication = "Journal of Neuroscience, (38), 2, _pp. 263-277_, https://doi.org/10.1523/JNEUROSCI.0322-17.2017"
publication_short = "Journal of Neuroscience, (38), 2, _pp. 263-277_, https://doi.org/10.1523/JNEUROSCI.0322-17.2017"
abstract = ""
abstract_short = ""
image_preview = ""
selected = true
projects = ['meta']
tags = []
url_pdf = "files/reprint_JNeuro2017meta.pdf"
url_preprint = "https://www.biorxiv.org/content/early/2016/12/21/095950"
url_code = ""
url_dataset = "https://osf.io/x2jws/"
url_project = "project/meta"
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
url_custom = [{name = "SI", url = "files/SI_JNeuro2017meta.docx"}]
math = true
highlight = true
[header]
image = ""
caption = ""
+++
