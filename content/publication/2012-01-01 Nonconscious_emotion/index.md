+++
title = "Nonconscious emotional processing involves distinct neural pathways for pictures and videos"
date = "2012-01-01"
authors = ["N. Faivre", "S. Charron", "P. Roux", "S. Lehericy", "S. Kouider"]
publication_types = ["1"]
publication = "Neuropsychologia, (50), 14, _pp. 3736-3744_, https://doi.org/10.1016/j.neuropsychologia.2012.10.025"
publication_short = "Neuropsychologia, (50), 14, _pp. 3736-3744_, https://doi.org/10.1016/j.neuropsychologia.2012.10.025"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious"]
tags = []
url_pdf = "files/reprint_NeuroPsy2012.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
