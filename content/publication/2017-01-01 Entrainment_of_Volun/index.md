+++
title = "Entrainment of Voluntary Movement to Undetected Auditory Regularities"
date = "2017-01-01"
authors = ["A. Schurger", "N. Faivre", "L. Cammoun", "B. Trovo", "O. Blanke"]
publication_types = ["1"]
publication = "Scientific Reports, (7), 1, https://doi.org/10.1038/s41598-017-15126-w"
publication_short = "Scientific Reports, (7), 1, https://doi.org/10.1038/s41598-017-15126-w"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious","multisensory"]
tags = []
url_pdf = "https://www.nature.com/articles/s41598-017-15126-w"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
