+++
title = "Nonconscious Learning From Crowded Sequences"
date = "2014-01-01"
authors = ["A. Atas", "N. Faivre", "B. Timmermans", "A. Cleeremans", "S. Kouider"]
publication_types = ["1"]
publication = "Psychological Science, (25), 1, _pp. 113-119_, https://doi.org/10.1177/0956797613499591"
publication_short = "Psychological Science, (25), 1, _pp. 113-119_, https://doi.org/10.1177/0956797613499591"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious"]
tags = []
url_pdf = "reprint_psychscience2013.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_custom = [{name = "SI", url = "files/reprint_psychscience2013_SI.pdf"}]
math = true
highlight = true
[header]
image = ""
caption = ""
+++
