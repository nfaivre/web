+++
title = "La conscience chez les humains"
date = "2018-01-10"
authors = ["Nathan Faivre", "Jacques Servière", "Patrick Prunet"]
publication_types = ["3"]
publication = "La conscience des animaux (QUAE, ISBN 978-2-7592-2870-6)"
publication_short = "La conscience des animaux (QUAE, ISBN 978-2-7592-2870-6)"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["animal"]
tags = []
url_pdf = "files/reprint_QUAE.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = "http://www.quae.com/fr/r5324-la-conscience-des-animaux.html"
math = true
highlight = true
[header]
image = ""
caption = ""
+++
