+++
title = "Animal Consciousness"
date = "2017-01-09"
authors = ["P. Le Neindre", "E. Bernard", "A. Boissy", "X. Boivin", "L. Calandreau", "N. Delon", "B. Deputte", "S. Desmoulin-Canselier", "M. Dunier", "N. Faivre", "M. Giurfa", "JL. Guichet", "L. Landade", "R. Larrere", "P. Mormède", "P. Prunet", "B. Schaal", "J. Servière", "C. Terlouw"]
publication_types = ["3"]
publication = "EFSA supporting publication"
publication_short = "EFSA supporting publication"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["animal"]
tags = []
url_pdf = "files/reprint_EFSA2017.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = "http://www.efsa.europa.eu/en/supporting/pub/en-1196"
math = true
highlight = true
[header]
image = ""
caption = ""
+++
