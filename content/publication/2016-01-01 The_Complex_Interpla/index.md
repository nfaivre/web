+++
title = "The Complex Interplay between Multisensory Integration and Perceptual Awareness"
date = "2016-01-01"
authors = ["O. Deroy", "N. Faivre", "C. Lunghi", "C. Spence", "M. Aller", "U. Noppeney"]
publication_types = ["1"]
publication = "Multisensory Research, (29), 6, _pp. 585-606_, https://doi.org/10.1163/22134808-00002529"
publication_short = "Multisensory Research, (29), 6, _pp. 585-606_, https://doi.org/10.1163/22134808-00002529"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["percept","multisensory"]
tags = []
url_pdf = "files/reprint_MultisensoryResearch2016.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
