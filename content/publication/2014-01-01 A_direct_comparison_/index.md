+++
title = "A direct comparison of unconscious face processing under masking and interocular suppression"
date = "2014-01-01"
authors = ["G. Izatt", "J. Dubois", "N. Faivre", "C. Koch"]
publication_types = ["1"]
publication = "Frontiers in Psychology, (5), JUL, https://doi.org/10.3389/fpsyg.2014.00659"
publication_short = "Frontiers in Psychology, (5), JUL, https://doi.org/10.3389/fpsyg.2014.00659"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious"]
tags = []
url_pdf = "https://www.frontiersin.org/articles/10.3389/fpsyg.2014.00659/full"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
