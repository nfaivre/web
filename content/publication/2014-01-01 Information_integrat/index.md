+++
title = "Information integration without awareness"
date = "2014-01-01"
authors = ["L. Mudrik", "N. Faivre", "C. Koch"]
publication_types = ["2"]
publication = "Trends in Cognitive Sciences, (18), 9, _pp. 488-496_, https://doi.org/10.1016/j.tics.2014.04.009"
publication_short = "Trends in Cognitive Sciences, (18), 9, _pp. 488-496_, https://doi.org/10.1016/j.tics.2014.04.009"
abstract = ""
abstract_short = ""
image_preview = ""
selected = true
projects = ["unconscious","multisensory"]
tags = []
url_pdf = "files/reprint_TICS2014.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
