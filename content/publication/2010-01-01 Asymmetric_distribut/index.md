+++
title = "Asymmetric distribution of epidermal growth factor receptor directs the fate of normal and cancer keratinocytes in vitro"
date = "2010-01-01"
authors = ["H. {Le Roy}", "T. Zuliani", "I. Wolowczuk", "N. Faivre", "N. Jouy", "B. Masselot", "J.-P. Kerkaert", "P. Formstecher", "R. Polakowska"]
publication_types = ["1"]
publication = "Stem Cells and Development, (19), 2, _pp. 209-220_, https://doi.org/10.1089/scd.2009.0150"
publication_short = "Stem Cells and Development, (19), 2, _pp. 209-220_, https://doi.org/10.1089/scd.2009.0150"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = []
url_pdf = "files/reprint_StemDev2010.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
