+++
title = "Sensorimotor conflicts alter perceptual and action monitoring"
date = "2018-12-21"
authors = ["N. Faivre", "L. Vuillaume", "F. Bernasconi", "R. Salomon", "O. Blanke", "A. Cleeremans"]
publication_types = ["1"]
publication = "bioRxiv, https://doi.org/10.1101/504274"
publication_short = "bioRxiv, https://doi.org/10.1101/504274"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ['meta']
tags = []
url_pdf = ""
url_preprint = "https://www.biorxiv.org/content/early/2018/12/21/504274"
url_code = "https://osf.io/386az/"
url_dataset = "https://osf.io/386az/"
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = false
[header]
image = ""
caption = ""
+++
