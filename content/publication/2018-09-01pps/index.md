+++
title = "Audio-tactile and peripersonal space processing around the trunk in human parietal and temporal cortex: an intracranial EEG study"
date = "2018-09-10"
authors = ["F. Bernasconi", "JP. Noel", "H. Park", "N. Faivre", "M. Seeck", "L. Spinelli", "K. Schaller", "O. Blanke"]
publication_types = ["1"]
publication = "Cerebral Cortex, 28(9), 3385–3397,https://doi.org/10.1093/cercor/bhy156"
publication_short = "Cerebral Cortex, 28(9), 3385–3397,https://doi.org/10.1093/cercor/bhy156"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["self","multisensory"]
tags = []
url_pdf = "files/reprint_CerCo2018.pdf"
url_preprint = "https://www.biorxiv.org/content/early/2018/01/18/249078"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = "https://academic.oup.com/cercor/article/28/9/3385/5052728"
url_custom = [{name = "SI", url = "https://oup.silverchair-cdn.com/oup/backfile/Content_public/Journal/cercor/28/9/10.1093_cercor_bhy156/2/bhy156_suppl1.zip?Expires=2147483647&Signature=w9HtpaJmQ~pJxs-wmG9GsEG-kSAURS8JhN3Mi06aXedbNyo4lu08uzdnD3KUISFKYw67nOsNuLc4gNIZZxTi333X3zn83agTN7K16GM3MrcgjgJFKVW7~0CWRz20cOM1HxbB3aTAv-EA6VqP6M-vP0KgnY~jBSrMjF14nZCDnm2e2TgZnMF48W65JFVYByZ1EWMYbZGGuw6RX~PjTX5Z-Qe6o8HoqR3DNBQCsahKxUPDJ5ZUnfGqewEyxRWX8KR8ZF-6j7GzojV-A3D5FA-koF9JtbnVl1Int4X6QYpikja5ReTDFCT6ufCIzwsL8~uUDyst4E8R76tZaISvHAwu6g__&Key-Pair-Id=APKAIE5G5CRDK6RD3PGA"}]
math = true
highlight = true
[header]
image = ""
caption = ""
+++
