+++
title = "Visual consciousness and bodily self-consciousness"
date = "2015-01-01"
authors = ["N. Faivre", "R. Salomon", "O. Blanke"]
publication_types = ["2"]
publication = "Current Opinion in Neurology, (28), 1, _pp. 23-28_, https://doi.org/10.1097/WCO.0000000000000160"
publication_short = "Current Opinion in Neurology, (28), 1, _pp. 23-28_, https://doi.org/10.1097/WCO.0000000000000160"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["percept","self"]
tags = []
url_pdf = "files/reprint_CurNeuro2015.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
