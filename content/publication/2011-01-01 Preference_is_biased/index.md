+++
title = "Preference is biased by crowded facial expressions"
date = "2011-01-01"
authors = ["S. Kouider", "V. Berthet", "N. Faivre"]
publication_types = ["1"]
publication = "Psychological Science, (22), 2, _pp. 184-189_, https://doi.org/10.1177/0956797610396226"
publication_short = "Psychological Science, (22), 2, _pp. 184-189_, https://doi.org/10.1177/0956797610396226"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious"]
tags = []
url_pdf = "files/reprint_psychscience2011.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
