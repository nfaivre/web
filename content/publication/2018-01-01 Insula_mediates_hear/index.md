+++
title = "Insula mediates heartbeat related effects on visual consciousness"
date = "2018-01-01"
authors = ["R. Salomon", "R. Ronchi", "J. Donz", "J. Bello-Ruiz", "B. Herbelin", "N. Faivre", "K. Schaller", "O. Blanke"]
publication_types = ["1"]
publication = "Cortex, (101), _pp. 87-95_, https://doi.org/10.1016/j.cortex.2018.01.005"
publication_short = "Cortex, (101), _pp. 87-95_, https://doi.org/10.1016/j.cortex.2018.01.005"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ['self.md']
tags = []
url_pdf = "files/reprint_Cortex2018.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
