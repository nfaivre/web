+++
title = "Enhanced discriminability for nonbiological motion violating the two-thirds power law"
date = "2016-01-01"
authors = ["R. Salomon", "A. Goldstein", "L. Vuillaume", "N. Faivre", "R.R. Hassin", "O. Blanke"]
publication_types = ["1"]
publication = "Journal of Vision, (16), 8, https://doi.org/10.1167/16.8.12"
publication_short = "Journal of Vision, (16), 8, https://doi.org/10.1167/16.8.12"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious"]
tags = []
url_pdf = "https://jov.arvojournals.org/article.aspx?articleid=2528619"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
