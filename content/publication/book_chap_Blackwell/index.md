+++
title = "Leaving Body and Life Behind: Out-of-Body and Near-Death Experience"
date = "2017-01-01"
authors = ["S. Kouider", "N. Faivre"]
publication_types = ["3"]
publication = "The Blackwell Companion to Consciousness (2nd edition)"
publication_short = "The Blackwell Companion to Consciousness (2nd edition)"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = []
url_pdf = "files/reprint_Blackwell2017.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = "https://www.wiley.com/en-us/The+Blackwell+Companion+to+Consciousness%2C+2nd+Edition-p-9780470674062"
math = true
highlight = true
[header]
image = ""
caption = ""
+++
