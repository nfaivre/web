+++
title = "Unconscious integration of multisensory bodily inputs in the peripersonal space shapes bodily self-consciousness"
date = "2017-01-01"
authors = ["R. Salomon", "J.-P. Noel", "M. Lukowska", "N. Faivre", "T. Metzinger", "A. Serino", "O. Blanke"]
publication_types = ["1"]
publication = "Cognition, (166), _pp. 174-183_, https://doi.org/10.1016/j.cognition.2017.05.028"
publication_short = "Cognition, (166), _pp. 174-183_, https://doi.org/10.1016/j.cognition.2017.05.028"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["multisensory","self"]
tags = []
url_pdf = "files/reprint_Cognition2017.pdf"
url_preprint = "https://www.biorxiv.org/content/early/2016/04/11/048108"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
