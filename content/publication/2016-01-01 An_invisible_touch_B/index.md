+++
title = "An invisible touch: Body-related multisensory conflicts modulate visual consciousness"
date = "2016-01-01"
authors = ["R. Salomon", "G. Galli", "M. Lukowska", "N. Faivre", "J.B. Ruiz", "O. Blanke"]
publication_types = ["1"]
publication = "Neuropsychologia, (88), _pp. 131-139_, https://doi.org/10.1016/j.neuropsychologia.2015.10.034"
publication_short = "Neuropsychologia, (88), _pp. 131-139_, https://doi.org/10.1016/j.neuropsychologia.2015.10.034"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = ["unconscious","self"]
tags = []
url_pdf = "files/reprint_Neuropsychologia2015.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
