+++
title = "Leaving Body and Life Behind: Out-of-Body and Near-Death Experience"
date = "2015-01-09"
authors = ["O. Blanke", "N. Faivre", "S. Dieguez"]
publication_types = ["3"]
publication = "The Neurology of Consciousness (2nd edition)"
publication_short = "The Neurology of Consciousness (2nd edition)"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = []
url_pdf = "files/reprint_NeurologyofConsciousness2015.pdf"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = "https://www.elsevier.com/books/the-neurology-of-consciousness/laureys/978-0-12-800948-2"
math = true
highlight = true
[header]
image = ""
caption = ""
+++
