+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 5

 
+++

# Biography
<br> 
Nathan obtained his Ph.D. at the [*Ecole Normale Supérieure*] (https://www.ens.fr/) in Paris with [*Sid Kouider*] (http://sidkouider.com/).  

He worked as a postdoc at the [*California Institute of Technology*] (http://www.caltech.edu) in Pasadena with [*Christof Koch*] (https://christofkoch.com), and at the [*Swiss Federal Institute of Technology*] (https://epfl.ch/) in Geneva with [*Olaf Blanke*] (https://lnco.epfl.ch/team/olaf-blanke-2/).

He started to work as a [*CNRS scientist*] (www.cnrs.fr) in 2016. After two years at the [*Centre d'Economie de la Sorbonne*] (https://centredeconomiesorbonne.univ-paris1.fr/) in Paris,  he is now working at the [*Laboratoire de Psychologie & Neurocognition*] (https://lpnc.univ-grenoble-alpes.fr) in Grenoble.  

Most of his present research is dedicated to [**metAction**] (../../project/meta/), an ERC-funded project which aims at documenting the contribution of sensorimotor signals to metacognition, in view of developping new remediation procedures.
