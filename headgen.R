## R tutorial for EEG data
## N. Faivre March 2018

# house keeping ----------------------------------------------------------- 
rm(list=ls(all=TRUE))
library(lme4);library(lmerTest) # mixed model stuff
library(Hmisc)
library(tidyverse); theme_set(theme_bw(base_size = 10)); 
library(afex)
library(multidplyr)
library(zoo)
library(eegUtils) # install with devtools::install_github("craddm/eegUtils")

rootdir = '/home/faivre/Documents/gitlab/ThalamusLNCO/Rtraining/EEG/' # get directory in which the script is saved
setwd(rootdir)

source('./functions/myload_set.R')
ci <- function (x) { 1.96*sd(x)/sqrt(length(x))}


locking    = 'resplocked'; # resplocked or stimlocked?
cond       = '' # maybe specify an experimental condition later


eeglist<-dir(paste(rootdir,'data',sep='/'), '*.set') # list EEG files

# Preprocessing -----------------------------------------------------------
  a=list()
  # read preprocessed files from EEGLAB iteratively and append to a
  for (f in 1:length(eeglist)) {
    EEG = myload_set(paste(rootdir,'data',eeglist[f],sep='/'))
    tmp = cbind(EEG$signals,EEG$timings[,1],EEG$timings[,2])
    tmp = tmp %>% gather(chan,amp,names(tmp)[1:63])
    
    events=as.data.frame(EEG$events)
    tmp=full_join(tmp,events, by ='epoch')
    tmp$suj=substr(f,1,3)
    a[[f]] = tmp # create a column for subject
  }
a=bind_rows(a)

a %>%
  select(amp,chan,time,epoch) %>%
  group_by(chan,time) %>%
  summarise(amp = mean(amp,na.rm=T)) %>%
  ggplot(aes(x = time, y = 4+amp^2,group=chan)) +
  geom_path(alpha=0.3,size=.2) + labs(x=c(''),y=c(''))+
  theme(panel.border = element_blank(), panel.grid.major = element_blank(),panel.grid.minor = element_blank(), 
        axis.line = element_blank(),axis.title.x=element_blank(),
        axis.text.x=element_blank(),
        axis.ticks.x=element_blank(),axis.title.y=element_blank(),
        axis.text.y=element_blank(),
        axis.ticks.y=element_blank(),
        panel.background = element_rect(fill = "#f7f7f7"),plot.margin = margin(0, 0, 0, 0, "cm"))
ggsave('/home/faivre/Documents/gitlab/web/static/img/head.png',device='png',width=34,height=3.85,units='cm')
